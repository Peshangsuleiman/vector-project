package additionalFunctions;

import state.*;

public class StateInsertTransparentShapes {

    private static StateInsertTransparentShapes instance = null;

    private StateInsertTransparentShapes() { }

    public void createTransparentShapes(){
        StateInsertCircle.getInstance().setTransparent(true);
        StateInsertRectangle.getInstance().setTransparent(true);
        StateInsertOval.getInstance().setTransparent(true);
        StateInsertSquare.getInstance().setTransparent(true);
        StateInsertTriangle.getInstance().setTransparent(true);
    }

    public static StateInsertTransparentShapes getInstance() {
        if (instance == null)
            instance = new StateInsertTransparentShapes();
        return instance;
    }
}
