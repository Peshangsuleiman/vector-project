package additionalFunctions;


import state.*;

public class StateInsertFilledShapes extends State {

    private static StateInsertFilledShapes instance = null;

    private StateInsertFilledShapes() { }

    public void createOpaqueShapes(){
        StateInsertCircle.getInstance().setTransparent(false);
        StateInsertRectangle.getInstance().setTransparent(false);
        StateInsertOval.getInstance().setTransparent(false);
        StateInsertSquare.getInstance().setTransparent(false);
        StateInsertTriangle.getInstance().setTransparent(false);
    }

    public static StateInsertFilledShapes getInstance() {
        if (instance == null)
            instance = new StateInsertFilledShapes();
        return instance;
    }
}
