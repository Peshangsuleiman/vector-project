/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

import state.StateCombine;

import java.awt.*;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public class ShapeDecoratorCrosshair extends AbstractShape implements Shape, Cloneable {

    private Shape shapeToDecorate;
    private final String TYPE = "SDC";

    public ShapeDecoratorCrosshair(Shape shapeToDecorate) {
        this.shapeToDecorate = shapeToDecorate;
        setMarkedCrosshair(TYPE);
    }

    @Override
    public void draw(Graphics2D g) {
        shapeToDecorate.draw(g);
        Point position = shapeToDecorate.getPosition();
        int x = (int) (position.getX() - shapeToDecorate.getWidth() / 2.0 + 0.5);
        int y = (int) (position.getY() - shapeToDecorate.getHeight() / 2.0 + 0.5);
        g.drawLine(x, (int) (position.getY()), x + (int) (shapeToDecorate.getWidth()), (int) (position.getY()));
        g.drawLine((int) (position.getX()), y, (int) (position.getX()), y + (int) (shapeToDecorate.getHeight()));
    }

    @Override
    public void setColor(Color color) { shapeToDecorate.setColor(color); }

    @Override
    public int getColor() { return shapeToDecorate.getColor(); }

    @Override
    public double getOriginalSideOrRadius() {
        return shapeToDecorate.getOriginalSideOrRadius();
    }

    @Override
    public Point getPosition() {
        return shapeToDecorate.getPosition();
    }

    @Override
    public double getWidth() {
        return shapeToDecorate.getWidth();
    }

    @Override
    public double getHeight() {
        return shapeToDecorate.getHeight();
    }

    @Override
    public boolean intersects(Point point) {
        return shapeToDecorate.intersects(point);
    }

    @Override
    public void moveTo(Point point) {
        if(isComposite && compositeShape.getCompositeShapes().size() > 1)
            compositeShape.move(point.getX(), point.getY());
        else
            shapeToDecorate.moveTo(point);
    }

    @Override
    public void moveOrDraw(double dx, double dy) {
        shapeToDecorate.moveOrDraw(dx, dy);
    }

    @Override
    public void reSizeTo(Point point) {
        shapeToDecorate.reSizeTo(point);
    }

    @Override
    public Shape peel() {
        return shapeToDecorate;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getSubType() { return shapeToDecorate.getSubType(); }

    @Override
    public void setMarkedShape(String marked) { shapeToDecorate.setMarkedShape(marked); }

    @Override
    public void setMarkedCrosshair(String marked) { shapeToDecorate.setMarkedCrosshair(marked); }

    @Override
    public String getMarkedShape() { return shapeToDecorate.getMarkedShape(); }

    @Override
    public String getMarkedCrosshair() { return shapeToDecorate.getMarkedCrosshair(); }

    @Override
    public void lineSize(int size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getLineSize() { return shapeToDecorate.getLineSize(); }

    @Override
    public void setIsTransparent(boolean isTransparent) { shapeToDecorate.setIsTransparent(isTransparent); }

    @Override
    public boolean getIsTransparent() { return shapeToDecorate.getIsTransparent(); }


    @Override
    public Shape shapeClone() {
        try {
            ShapeDecoratorCrosshair shapeDecoratorCrosshair = (ShapeDecoratorCrosshair) super.clone();
            shapeDecoratorCrosshair.shapeToDecorate = (Shape) shapeToDecorate.shapeClone();
            shapeDecoratorCrosshair.compositeShape = null;
            shapeDecoratorCrosshair.setIsCompositeItem(false);
            return shapeDecoratorCrosshair;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}