/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

import state.StateCombine;

import java.awt.*;


/**
 * @author 
 */
public interface Shape {
    void draw(Graphics2D g);
    void setColor(Color color);
    int getColor();
    double getOriginalSideOrRadius();
    Point getPosition();
    double getWidth();
    double getHeight();
    boolean intersects(Point point);
    void moveTo(Point point);
    void moveOrDraw(double dx, double dy);
    void reSizeTo(Point point);
    void lineSize(int size);
    int getLineSize();
    Shape peel();
    String getType();
    String getSubType();
    void setMarkedShape(String marked);
    void setMarkedCrosshair(String marked);
    String getMarkedShape();
    String getMarkedCrosshair();
    void setCompositeList(StateCombine compositeShape);
    StateCombine getCompositeList();
    void setIsCompositeItem(boolean isComposite);
    boolean getIsCompositeItem();
    void setIsTransparent(boolean isTransparent);
    boolean getIsTransparent();
    Shape shapeClone();
}

