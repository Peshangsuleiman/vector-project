package shapes;

public class ShapeFactory {

    private static final ShapeFactory INSTANCE = new ShapeFactory();

    private ShapeFactory(){}

    public Circle createCircle(Point point, double r){ return new Circle(point, r); }

    public Circle createCircle(double x, double y, double r){
        return new Circle(x, y, r);
    }

    public Oval createOval(Point point, double r){
        return new Oval(point, r);
    }

    public Oval createOval(double x, double y, double r){
        return new Oval(x, y, r);
    }

    public Triangle createTriangle(Point point, double r){
        return new Triangle(point, r);
    }

    public Triangle createTriangle(double x, double y, double r){
        return new Triangle(x, y, r);
    }

    public Rectangle createRectangle(Point point, double r){ return new Rectangle(point, r); }

    public Rectangle createRectangle(double x, double y, double r){
        return new Rectangle(x, y, r);
    }

    public Square createSquare(Point point, double r){ return new Square(point, r); }

    public Square createSquare(double x, double y, double r){
        return new Square(x, y, r);
    }

    public ShapeDecorator createShapeDecorator(Shape shape){ return new ShapeDecorator(shape); }

    public ShapeDecoratorCrosshair createShapeDecoratorCrosshair(Shape shape){return new ShapeDecoratorCrosshair(shape); }

    public Pixel createPixel(int x, int y, int color){ return new Pixel(x, y , color);}

    public Point createPoint(double x, double y){ return new Point(x, y); }

    public static ShapeFactory getFactoryInstance(){return INSTANCE;}
}
