package shapes;

import state.StateCombine;

import java.awt.*;

//if statment in move that checks if the shape is composite then if yes calls the move in the compositeshape object else just moves
//state split sets the iscomposite variable to false combine sets it to true.
//memory leak here... if a shape is cloned at some point due to a undo command and the shape is a combined shape.
//the clone will be set as not being a combined shape but still a reference to it will be located in a combined shape object... hence that object will not be cleaned away.
//hence the clone will need a new compositState...
public class Circle extends AbstractShape implements Shape, Cloneable {
    private double r;
    private final String TYPE = "CIRCLE";

    public Circle(double x, double y, double r) {
        super.center = ShapeFactory.getFactoryInstance().createPoint(x, y);
        this.r = r;
    }

    public Circle(Point point, double r) {
        this(point.getX(), point.getY(), r);
    }

    //The g here is the JPanel to draw onto, sent from the PaintComponent method of the ShapeContainer.
    //In conclusion since Graphics in a abstract base class, all these draw methods are JPanels own methods.
    //make resize save the previous shape first.
    @Override
    public void draw(Graphics2D g) {
        g.setStroke(new BasicStroke(getLineSize()));
        if (isTransparent) {
            g.setColor(super.color);
            g.drawOval((int) (0.5 + center.getX() - r), (int) (0.5 + center.getY() - r), (int) (0.5 + 2 * r), (int) (0.5 + 2 * r));
        } else {
            g.setColor(super.color);
            g.fillOval((int) (0.5 + center.getX() - r), (int) (0.5 + center.getY() - r), (int) (0.5 + 2 * r), (int) (0.5 + 2 * r));
        }
        if (super.color.equals(Color.WHITE)) {
            g.setStroke(new BasicStroke(getLineSize()));
            g.setColor(Color.BLACK);
            g.drawOval((int) (0.5 + center.getX() - r), (int) (0.5 + center.getY() - r), (int) (0.5 + 2 * r), (int) (0.5 + 2 * r));
        }
    }

    @Override
    public double getOriginalSideOrRadius() { return r; }

    @Override
    public double getWidth() { return 2 * r; }

    @Override
    public double getHeight() { return 2 * r; }

    @Override
    public boolean intersects(Point point) { return center.distanceTo(point) < r; }

    @Override
    public void moveTo(Point point) {
        if (isComposite && compositeShape.getCompositeShapes().size() > 1)
            compositeShape.move(point.getX(), point.getY());
        else
            center.moveTo(point);
    }

    @Override
    public void moveOrDraw(double dx, double dy) { center.move(dx, dy); }

    @Override
    public void reSizeTo(Point point) { r = center.distanceTo(point); }

    @Override
    public String getType() { return TYPE; }

    @Override
    public String getSubType() { return TYPE; }

    //same classes have access to each others private variables.
    //hence that's why we can access the new circles center variable even though its private.
    @Override
    public Shape shapeClone() {
        try {
            Circle circle = (Circle) super.clone();
            circle.center = new Point(center.getX(), center.getY());
            circle.color = this.color;
            circle.compositeShape = null;    //Any issues with undo/redo command can be caused by this line right here. remove if needed.
            circle.setIsCompositeItem(false);
            return circle;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}