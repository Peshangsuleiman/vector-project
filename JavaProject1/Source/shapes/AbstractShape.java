package shapes;

import state.StateCombine;

import java.awt.*;

//all shapes will extend this class to make them less filled with clutter.
public abstract class  AbstractShape implements Shape{

    protected String markedShape = "N";
    protected String markedCorssHair = "N";
    protected int lineSize;
    protected boolean isComposite = false;
    protected StateCombine compositeShape;
    protected Color color = Color.WHITE;
    protected boolean isTransparent = true;
    protected Point center;

    @Override
    public void setColor(Color color) { this.color = color; }

    @Override
    public int getColor() { return color.getRGB(); }

    @Override
    public Point getPosition() { return center; }

    @Override
    public Shape peel() { return this; }

    @Override
    public void setMarkedShape(String marked) { markedShape = marked; }

    @Override
    public void setMarkedCrosshair(String marked) { markedCorssHair = marked; }

    @Override
    public String getMarkedShape() { return markedShape; }

    @Override
    public String getMarkedCrosshair() { return markedCorssHair; }

    @Override
    public void lineSize(int size) { lineSize = size; }

    @Override
    public int getLineSize() { return lineSize; }

    @Override
    public StateCombine getCompositeList() { return compositeShape; }

    @Override
    public void setIsCompositeItem(boolean isComposite) { this.isComposite = isComposite; }

    @Override
    public boolean getIsCompositeItem() { return isComposite; }

    @Override
    public void setIsTransparent(boolean isTransparent) { this.isTransparent = isTransparent; }

    @Override
    public boolean getIsTransparent() { return isTransparent; }

    @Override
    public void setCompositeList(StateCombine compositeShape) { this.compositeShape = compositeShape; }
}
