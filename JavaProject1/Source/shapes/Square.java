/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

import state.StateCombine;

import java.awt.*;

/**
 * @author pesh
 */
public class Square extends AbstractShape implements Shape, Cloneable{

    private double side;
    private final String TYPE = "SQUARE";

    public Square(double x, double y, double side) {
        super.center = ShapeFactory.getFactoryInstance().createPoint(x, y);
        this.side = side;
    }

    public Square(Point point, double side) {
        this(point.getX(), point.getY(), side);
    }

    @Override
    public void draw(Graphics2D g) {
        g.setStroke(new BasicStroke(getLineSize()));
        if (isTransparent) {
            g.setColor(super.color);
            g.drawRect((int) (center.getX()-side/2), (int) (center.getY()-side/2), (int) (side), (int) (side));
        } else {
            g.setColor(super.color);
            g.fillRect((int) (center.getX() - side / 2), (int) (center.getY() - side / 2), (int) (side), (int) (side));
        }
        if(super.color.equals(Color.WHITE)) {
            g.setColor(Color.BLACK);
            g.drawRect((int) (center.getX()-side/2), (int) (center.getY()-side/2), (int) (side), (int) (side));
        }
    }

    @Override
    public double getOriginalSideOrRadius() {
        return side;
    }

    @Override
    public double getWidth() {
        return side;
    }

    @Override
    public double getHeight() {
        return side;
    }

    @Override
    public boolean intersects(Point point) {
        return center.distanceTo(point) < side;
    }

    @Override
    public void moveTo(Point point) {
        if(isComposite && compositeShape.getCompositeShapes().size() > 1)
            compositeShape.move(point.getX(), point.getY());
        else
            center.moveTo(point);
    }

    @Override
    public void moveOrDraw(double dx, double dy) {
        center.move(dx, dy);
    }

    @Override
    public void reSizeTo(Point point) {
        side = center.distanceTo(point);
    }

    @Override
    public String getType() { return TYPE; }

    @Override
    public String getSubType() { return TYPE; }

    @Override
    public Shape shapeClone() {
        try {
            Square square = (Square) super.clone();
            square.center = new Point(center.getX(), center.getY());
            square.color = this.color;
            square.compositeShape = null;
            square.setIsCompositeItem(false);
            return square;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
