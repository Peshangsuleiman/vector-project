/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

import state.StateCombine;

import java.awt.*;

/**
 * @author pesh
 */
public class Oval extends AbstractShape implements Shape, Cloneable {

    private double r;
    private final String TYPE = "OVAL";

    public Oval(double x, double y, double r) {
        super.center = ShapeFactory.getFactoryInstance().createPoint(x, y);
        this.r = r;
    }

    public Oval(Point point, double r) {
        this(point.getX(), point.getY(), r);
    }

    @Override
    public void draw(Graphics2D g) {
        g.setStroke(new BasicStroke(getLineSize()));
        if (isTransparent) {
            g.setColor(super.color);
            g.drawOval((int) (0.5 + center.getX() - r), (int) (0.5 + center.getY() - r), (int) (0.5 + 3 * r), (int) (0.5 + 2 * r));
        } else {
            g.setColor(super.color);
            g.fillOval((int) (0.5 + center.getX() - r), (int) (0.5 + center.getY() - r), (int) (0.5 + 3 * r), (int) (0.5 + 2 * r));
        }
        if(super.color.equals(Color.WHITE)) {
            g.setColor(Color.BLACK);
            g.drawOval((int) (0.5 + center.getX() - r), (int) (0.5 + center.getY() - r), (int) (0.5 + 3 * r), (int) (0.5 + 2 * r));
        }
    }

    @Override
    public double getOriginalSideOrRadius() {
        return r;
    }

    @Override
    public double getWidth() {
        return 3 * r;
    }

    @Override
    public double getHeight() {
        return 2 * r;
    }

    @Override
    public boolean intersects(Point point) {
        return center.distanceTo(point) < r;
    }

    @Override
    public void moveTo(Point point) {
        if(isComposite && compositeShape.getCompositeShapes().size() > 1)
            compositeShape.move(point.getX(), point.getY());
        else
            center.moveTo(point);
    }

    @Override
    public void moveOrDraw(double dx, double dy) {
        center.move(dx, dy);
    }

    @Override
    public void reSizeTo(Point point) {
        r = center.distanceTo(point);
    }

    @Override
    public String getType() { return TYPE; }

    @Override
    public String getSubType() { return TYPE; }

    @Override
    public Shape shapeClone() {
        try {
            Oval oval = (Oval) super.clone();
            oval.center = new Point(center.getX(), center.getY());
            oval.color = this.color;
            oval.compositeShape = null;
            oval.setIsCompositeItem(false);
            return oval;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
