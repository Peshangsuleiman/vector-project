/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;


import state.StateCombine;

import java.awt.*;

/**
 * @author pesh
 */
public class Triangle extends AbstractShape implements Shape, Cloneable {

    private double r;
    private final String TYPE = "TRIANGLE";

    public Triangle(double x, double y, double r) {
        super.center = ShapeFactory.getFactoryInstance().createPoint(x, y);
        this.r = r;
    }

    public Triangle(Point point, double r) {
        this(point.getX(), point.getY(), r);
    }

    @Override
    public void draw(Graphics2D g) {
        g.setStroke(new BasicStroke(getLineSize()));
        int[] Xcoord = {(int)center.getX(), ((int)center.getX() + (int)r/2), ((int)center.getX() - (int)r/2)}; 
        int[] Ycoord = {(int)center.getY(), (int)center.getY() + (int)r, (int)(center.getY() + (int)r)};

        if (isTransparent) {
            g.setColor(super.color);
            g.drawPolygon(Xcoord, Ycoord, 3);
        } else {
            g.setColor(super.color);
            g.fillPolygon(Xcoord, Ycoord, 3);
        }
        if(super.color.equals(Color.WHITE)) {
            g.setColor(Color.BLACK);
            g.drawPolygon(Xcoord, Ycoord, 3);
        }
    }

    @Override
    public double getOriginalSideOrRadius() {
        return r;
    }

    @Override
    public double getWidth() {
        return r;
    }

    @Override
    public double getHeight() {
        return r;
    }

    @Override
    public boolean intersects(Point point) {
        return center.distanceTo(point) < r;
    }

    @Override
    public void moveTo(Point point) {
        if(isComposite && compositeShape.getCompositeShapes().size() > 1)
            compositeShape.move(point.getX(), point.getY());
        else
            center.moveTo(point);
    }

    @Override
    public void moveOrDraw(double dx, double dy) {
        center.move(dx, dy);
    }

    @Override
    public void reSizeTo(Point point) {
        r = center.distanceTo(point);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getSubType() { return TYPE; }

    @Override
    public Shape shapeClone() {
        try {
            Triangle triangle = (Triangle) super.clone();
            triangle.center = new Point(center.getX(), center.getY());
            triangle.color = this.color;
            triangle.compositeShape = null;
            triangle.setIsCompositeItem(false);
            return triangle;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}