package shapes;

public class Pixel implements Cloneable {

    private int x;
    private int y;
    private int color;

    public Pixel(int x, int y, int color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setPosition(int x, int y){
        setX(x);
        setY(y);
    }

    @Override
    protected Pixel clone() {
        try {
            Pixel pixel = (Pixel) super.clone();
            return pixel;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
