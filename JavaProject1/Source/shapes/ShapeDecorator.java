package shapes;


import state.StateCombine;

import java.awt.*;

/**
 *
 */
public class ShapeDecorator extends AbstractShape implements Shape, Cloneable {

    private Shape shapeToDecorate;
    private final String TYPE = "SD";

    public ShapeDecorator(Shape shapeToDecorate) {
        this.shapeToDecorate = shapeToDecorate;
        setMarkedShape(TYPE);
    }

    @Override
    public void draw(Graphics2D g) {
        //Draws original shape first.
        //All is saved as one new (ShapeDecorator) shape hence why it moves as one with the move function.
        //However only the ShapeToDecorate is technically moved with the move function, the filled oval is just repainted based on the
        //New position of the ShapeToDecorate (the shape sent in).
        //In reality both are repainted but since both are in the same Shape object they seem and act like one shape.
        //Obs that the draw function is first called upon in the shapeContainer and the Graphics object
        // is its underlying JPanel graphics object
        shapeToDecorate.draw(g);
        Point position = shapeToDecorate.getPosition();
        int x = (int) (position.getX() - shapeToDecorate.getWidth() / 2.0 + 0.5);
        int y = (int) (position.getY() - shapeToDecorate.getHeight() / 2.0 + 0.5);
        g.fillOval(x, y, (int) (shapeToDecorate.getWidth() / 2), (int) (shapeToDecorate.getHeight() / 2));
    }

    @Override
    public void setColor(Color color) { shapeToDecorate.setColor(color); }

    @Override
    public int getColor() { return shapeToDecorate.getColor(); }

    @Override
    public double getOriginalSideOrRadius() {
        return shapeToDecorate.getOriginalSideOrRadius();
    }

    @Override
    public Point getPosition() {
        return shapeToDecorate.getPosition();
    }

    @Override
    public double getWidth() {
        return shapeToDecorate.getWidth();
    }

    @Override
    public double getHeight() {
        return shapeToDecorate.getHeight();
    }

    @Override
    public boolean intersects(Point point) {
        return shapeToDecorate.intersects(point);
    }

    @Override
    public void moveTo(Point point) {
        if(isComposite && compositeShape.getCompositeShapes().size() > 1)
            compositeShape.move(point.getX(), point.getY());
        else
            shapeToDecorate.moveTo(point);
    }

    @Override
    public void moveOrDraw(double dx, double dy) {
        shapeToDecorate.moveOrDraw(dx, dy);
    }

    @Override
    public void reSizeTo(Point point) {
        shapeToDecorate.reSizeTo(point);
    }

    @Override
    public Shape peel() {
        return shapeToDecorate;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getSubType() { return shapeToDecorate.getSubType(); }

    @Override
    public void setMarkedShape(String marked) { shapeToDecorate.setMarkedShape(marked); }

    @Override
    public void setMarkedCrosshair(String marked) { shapeToDecorate.setMarkedCrosshair(marked); }

    @Override
    public String getMarkedShape() { return shapeToDecorate.getMarkedShape(); }

    @Override
    public String getMarkedCrosshair() { return shapeToDecorate.getMarkedCrosshair(); }

    @Override
    public void lineSize(int size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getLineSize() { return shapeToDecorate.getLineSize(); }

    @Override
    public void setIsTransparent(boolean isTransparent) { shapeToDecorate.setIsTransparent(isTransparent); }

    @Override
    public boolean getIsTransparent() { return shapeToDecorate.getIsTransparent(); }


    @Override
    public Shape shapeClone() {
        try {
            ShapeDecorator shapeDecorator = (ShapeDecorator) super.clone();
            shapeDecorator.shapeToDecorate = (Shape) shapeToDecorate.shapeClone();
            shapeDecorator.compositeShape = null;
            shapeDecorator.setIsCompositeItem(false);
            return shapeDecorator;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}