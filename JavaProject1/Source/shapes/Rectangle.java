 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package shapes;

 import state.StateCombine;

 import java.awt.*;

 /**
  * @author Peshang Suleiman & Ali Shakeri
  */
 public class Rectangle extends AbstractShape implements Shape, Cloneable {

     private double r;
     private final String TYPE = "RECTANGLE";

     public Rectangle(double x, double y, double r) {
         super.center = ShapeFactory.getFactoryInstance().createPoint(x, y);
         this.r = r;
     }

     public Rectangle(Point point, double r) {
         this(point.getX(), point.getY(), r);
     }

     @Override
     public void draw(Graphics2D g) {
         g.setStroke(new BasicStroke(getLineSize()));
         if (isTransparent) {
             g.setColor(super.color);
             g.drawRect((int) (0.5 + center.getX()-r*1.5), (int) (0.5 + center.getY()-r*1), (int) (0.5 + 3 * r), (int) (0.5 + 2 * r));
         } else {
            g.setColor(super.color);
            g.fillRect((int) (0.5 + center.getX()-r*1.5), (int) (0.5 + center.getY()-r*1), (int) (0.5 + 3 * r), (int) (0.5 + 2 * r));
         }
         if(super.color.equals(Color.WHITE)) {
            g.setColor(Color.BLACK);
            g.drawRect((int) (0.5 + center.getX()-r*1.5), (int) (0.5 + center.getY()-r*1), (int) (0.5 + 3 * r), (int) (0.5 + 2 * r));
        }
     }

     @Override
     public double getOriginalSideOrRadius() {
         return r;
     }

     @Override
     public double getWidth() {
         return 3 * r;
     }

     @Override
     public double getHeight() {
         return 2 * r;
     }

     @Override
     public boolean intersects(Point point) {
         return center.distanceTo(point) < r;
     }

     @Override
     public void moveTo(Point point) {
         if(isComposite && compositeShape.getCompositeShapes().size() > 1)
             compositeShape.move(point.getX(), point.getY());
         else
             center.moveTo(point);
     }

     @Override
     public void moveOrDraw(double dx, double dy) {
         //center.move(dx, dy);
     }

     @Override
     public void reSizeTo(Point point) {
         r = center.distanceTo(point);
     }

     @Override
     public String getType() { return TYPE; }

     @Override
     public String getSubType() { return TYPE; }

     @Override
     public Shape shapeClone() {
         try {
             Rectangle rectangle = (Rectangle) super.clone();
             rectangle.center = new Point(center.getX(), center.getY());
             rectangle.color = this.color;
             rectangle.compositeShape = null;
             rectangle.setIsCompositeItem(false);
             return rectangle;
         } catch (CloneNotSupportedException ex) {
             ex.printStackTrace();
             return null;
         }
     }
 }
