package shapes;

import state.StateCombine;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class LineFreeHand extends AbstractShape implements Shape, Cloneable {

    private List<Pixel> pixels = new ArrayList<>();
    private final String TYPE = "FREESHAPE";

    //needs a intersects to move hence we need a center and a radius around it.

    @Override
    public void draw(Graphics2D g) {
        g.setStroke(new BasicStroke(lineSize));
        for (Pixel pixel: pixels) {
            g.setColor(new Color(pixel.getColor()));
            g.drawLine(pixel.getX(), pixel.getY(), pixel.getX(), pixel.getY());
        }
    }


    @Override
    public void setColor(Color color) { super.color = color; }

    @Override
    public int getColor() {
        return super.color.getRGB();
    }

    @Override
    public double getOriginalSideOrRadius() {
        return 0;
    }

    //position of first pixel in list.
    @Override
    public Point getPosition() { return null; }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public boolean intersects(Point point) {
        return false;
    }

    @Override
    public void moveTo(Point point) { }

    @Override
    public void moveOrDraw(double x, double y) {
        pixels.add(ShapeFactory.getFactoryInstance().createPixel((int)(x), (int)(y), getColor()));
    }

    @Override
    public void reSizeTo(Point point) {}

    @Override
    public void lineSize(int size) {
        lineSize = size;
    }

    @Override
    public int getLineSize() {
        return lineSize;
    }

    @Override
    public Shape peel() {
        return this;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getSubType() {
        return TYPE;
    }

    @Override
    public void setMarkedShape(String marked) {

    }

    @Override
    public void setMarkedCrosshair(String marked) {

    }

    @Override
    public String getMarkedShape() {
        return null;
    }

    @Override
    public String getMarkedCrosshair() {
        return null;
    }

    @Override
    public void setCompositeList(StateCombine compositeShape) {

    }

    @Override
    public StateCombine getCompositeList() {
        return null;
    }

    @Override
    public void setIsCompositeItem(boolean isComposite) {

    }

    @Override
    public boolean getIsCompositeItem() {
        return false;
    }

    @Override
    public void setIsTransparent(boolean isTransparent) {
    }

    @Override
    public boolean getIsTransparent() {
        return false;
    }

    @Override
    public Shape shapeClone() {
        try {
            LineFreeHand lineFreeHand = (LineFreeHand) super.clone();
            List<Pixel> newList = new ArrayList<>();
            for (Pixel pixel : pixels) {
                newList.add(pixel.clone());
            }
            lineFreeHand.pixels = newList;
            return lineFreeHand;
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
