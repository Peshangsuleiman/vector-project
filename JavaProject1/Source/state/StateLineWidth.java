/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import model.ShapeContainer;

/**
 *
 * @author pesh
 */
public class StateLineWidth extends State{
    
    private static StateLineWidth instance = null;
    private int size;
    
    private StateLineWidth(){
    }
    
    public void setLineSize(int size){
        this.size = size;
        StateFreeDraw.getInstance().setLinesize(size);
    }
    
    public void pointerDown(Point point, ShapeContainer shapeContainer){
        shapeContainer.select(point);
        if(shapeContainer.selected != null){
            addToUndoList();
              shapeContainer.selected.lineSize(size);
        }
        shapeContainer.repaint();
    }
    
    public static StateLineWidth getInstance(){
        if(instance == null)
            instance = new StateLineWidth();
        return instance; 
    }
} 
