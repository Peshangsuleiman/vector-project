package state;

import model.ShapeContainer;
import shapes.Point;
import shapes.Shape;

import java.util.ArrayList;
import java.util.List;

public class StateCombine extends State{

    private List<Shape> compositeShapesList;
    private List<double[]> coordinatesRelativeToFirstShape;
    private int counter = 0;

    //OBS NEEDS TO BE CLONEABLE!
    public StateCombine(){
        compositeShapesList = new ArrayList<>();
        coordinatesRelativeToFirstShape = new ArrayList<>();;
    }


    public void pointerDown(Point point, ShapeContainer shapeContainer)  {
        shapeContainer.select(point);
        if (shapeContainer.selected != null) {
            shapeContainer.selected.setIsCompositeItem(true);
            addShapes(shapeContainer.selected);
            shapeContainer.selected.setCompositeList(this);
        }
    }


    public void addShapes(Shape shape){
        double[] relationCoordinates = new double[2];
        compositeShapesList.add(shape);
        relationCoordinates[0] = (compositeShapesList.get(0).getPosition().getX() - shape.getPosition().getX()); //diff in X
        relationCoordinates[1] = (compositeShapesList.get(0).getPosition().getY() - shape.getPosition().getY());//diff in Y
        coordinatesRelativeToFirstShape.add(relationCoordinates);
        //compare each shape to first shape in list here and take the first shapes X minus the new shapes X and the same for Y here and and add the values to a 2D array.
    }

    public List<Shape> getCompositeShapes(){ return compositeShapesList; }

    public void move(double x, double y){
        for (Shape shape: compositeShapesList) {
            shape.getPosition().moveTo(x - (coordinatesRelativeToFirstShape.get(counter)[0]), y - (coordinatesRelativeToFirstShape.get(counter)[1]));
            counter++;
        }
        counter = 0;
        //move all shapes in list relaitve to the first shape or the shape clicked on
    }


}
