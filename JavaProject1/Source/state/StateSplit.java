package state;

import model.ShapeContainer;
import shapes.Point;

public class StateSplit extends State {

    private static StateSplit instance = null;

    private StateSplit() {}
    //Issue here when 2 or more shapes are added to a combined shape and then one of the shapes from the combined shape is deleted and then the delete command
    //is undone with undo none of the shapes can be moved any more.
    //this is cause the shapes move relative to eachother.
    //make delete remove the shape as a whole. (alla inkluded shapes)
    public void pointerDown(Point point, ShapeContainer shapeContainer)  {
        shapeContainer.select(point);
        if (shapeContainer.selected != null) {
            shapeContainer.selected.getCompositeList().getCompositeShapes().remove(shapeContainer.selected);
            if(shapeContainer.selected.getCompositeList().getCompositeShapes().size() == 0) {
                shapeContainer.selected.setCompositeList(null);
                shapeContainer.selected.setIsCompositeItem(false);
            }
        }
    }

    public static StateSplit getInstance() {
        if (instance == null)
            instance = new StateSplit();
        return instance;
    }
}
