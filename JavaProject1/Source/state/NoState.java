package state;

/*
*
* @author Peshang Suleiman & Ali Shakeri
*/
public class NoState extends State {

    private static NoState instance = null;

    public static NoState getInstance(){
        if(instance == null)
            instance = new NoState();
        return instance;
    }
}
