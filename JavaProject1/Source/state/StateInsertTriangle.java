/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import model.ShapeContainer;
import shapes.ShapeFactory;

/**
 *
 * @author pesh
 */
public class StateInsertTriangle extends State {
    
    private static StateInsertTriangle instance = null;

    public void setTransparent(boolean isTransparent){ super.isTransparent = isTransparent; }

    private StateInsertTriangle(){}
    
    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        addToUndoList();
        shapeContainer.getShapes().add(ShapeFactory.getFactoryInstance().createTriangle(point, 100));
        shapeContainer.getLastShape().setIsTransparent(isTransparent);
        shapeContainer.repaint();
    }
    
    public static StateInsertTriangle getInstance(){
        if(instance == null)
            instance = new StateInsertTriangle();
        return instance; 
    }
}
