/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import model.ShapeContainer;
import shapes.Shape;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateDelete extends State {

    private static StateDelete instance = null;

    private StateDelete() {}

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
        if (shapeContainer.selected != null) {
            addToUndoList();
            if (shapeContainer.selected.getIsCompositeItem()) {
                for (Shape shape : shapeContainer.selected.getCompositeList().getCompositeShapes()) {
                    shapeContainer.getShapes().remove(shape);
                }
            } else {
                shapeContainer.getShapes().remove(shapeContainer.selected);
            }
            shapeContainer.repaint();
        }
    }

    public static StateDelete getInstance() {
        if (instance == null)
            instance = new StateDelete();
        return instance;
    }
}

