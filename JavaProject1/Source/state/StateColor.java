package state;

import model.ShapeContainer;
import shapes.Point;

import java.awt.*;

public class StateColor extends State {

    private static StateColor instance = null;
    private Color color;

    private StateColor(){ }

    public void setColor(Color color) {
        this.color = color;
        StateFreeDraw.getInstance().setColor(color);
    }

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
        if (shapeContainer.selected != null) {
            addToUndoList();
            shapeContainer.selected.setColor(color);
        }
        shapeContainer.repaint();
    }

    public static StateColor getInstance(){
        if(instance == null)
            instance = new StateColor();
        return instance;
    }
}
