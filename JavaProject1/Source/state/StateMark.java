
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import shapes.Shape;
import model.ShapeContainer;
import shapes.ShapeDecorator;
import shapes.ShapeFactory;

/**
 *
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateMark extends State {
   
    private static StateMark instance = null;
    
    private StateMark(){}

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
        if(shapeContainer.selected != null){
            addToUndoList();
            Shape markedShape = ShapeFactory.getFactoryInstance().createShapeDecorator(shapeContainer.selected);
            shapeContainer.getShapes().remove(shapeContainer.selected);
            shapeContainer.getShapes().add(markedShape);
            shapeContainer.repaint();
        }
    }
    
    public static StateMark getInstance(){
        if(instance == null)
            instance = new StateMark();
        return instance;
    }
}
