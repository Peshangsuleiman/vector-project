/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import model.*;
import shapes.Point;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateResize extends State {

    private static StateResize instance = null;


    private StateResize() {
    }

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
    }


    public void pointerMoved(Point point, boolean pointerDown, ShapeContainer shapeContainer) {

        if (shapeContainer.selected != null && pointerDown) {
            if (super.flag && shapeContainer.selected.getPosition().distanceTo(point) != 0) { //när resixe släpps ta ny bild o skicka in med resize obejct i bilden ska bara resize va med inte gamla. förra bilden har gamla
                addToUndoList();//Send in the previous shape that is on canvas but has not been sent in yet
                super.flag = false;
            }
            shapeContainer.selected.reSizeTo(point);//resize adds a new shape of the size to the shape container when the key is released. Maybe only get last shape it returns.
            shapeContainer.repaint();
        }
    }

    public void pointerUp(Point point, ShapeContainer shapeContainer) { super.flag = true; }

    public static StateResize getInstance() {
        if (instance == null)
            instance = new StateResize();
        return instance;
    }
}
