package state;

import model.ShapeContainer;
import shapes.Point;


public class StateSetTransparency extends State {


    private static StateSetTransparency instance = null;

    private StateSetTransparency() { }

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
        if (shapeContainer.selected != null && !shapeContainer.selected.getIsTransparent()) {
            addToUndoList();
            shapeContainer.selected.setIsTransparent(true);
        } else if (shapeContainer.selected != null && shapeContainer.selected.getIsTransparent()) {
            addToUndoList();
            shapeContainer.selected.setIsTransparent(false);
        }
        shapeContainer.repaint();
    }


    public static StateSetTransparency getInstance() {
        if (instance == null)
            instance = new StateSetTransparency();
        return instance;
    }
}
