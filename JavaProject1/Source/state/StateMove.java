/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import model.ShapeContainer;
import shapes.Shape;

/**
 *
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateMove extends State{
    
    private static StateMove instance = null;
    
    private StateMove(){}
    
    public void pointerDown(Point point, ShapeContainer shapeContainer){
        shapeContainer.select(point);
    }
    
    public void pointerMoved(Point point, boolean pointerDown, ShapeContainer shapeContainer) {

        if(shapeContainer.selected != null && pointerDown){
            if (super.flag && shapeContainer.selected.getPosition().distanceTo(point) != 0) {
                addToUndoList();
                super.flag = false;
            }
            shapeContainer.selected.moveTo(point);
            shapeContainer.repaint();
        }
    }

    public void pointerUp(Point point, ShapeContainer shapeContainer) { super.flag = true; }
    
    public static StateMove getInstance(){
       if(instance == null)
           instance = new StateMove();
       return instance; 
    }
}