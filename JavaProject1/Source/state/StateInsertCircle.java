/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Circle;
import shapes.Point;
import model.ShapeContainer;
import shapes.ShapeFactory;

/**
 *
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateInsertCircle extends State {
    
    private static StateInsertCircle instance = null;


    private StateInsertCircle(){}

    public void setTransparent(boolean isTransparent){
        super.isTransparent = isTransparent;
    }

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        addToUndoList();
        shapeContainer.getShapes().add(ShapeFactory.getFactoryInstance().createCircle(point, 50.0));
        shapeContainer.getLastShape().setIsTransparent(isTransparent);
        shapeContainer.repaint();
    }
    
    public static StateInsertCircle getInstance(){
        if(instance == null)
            instance = new StateInsertCircle();
            return instance;   
    }
}
