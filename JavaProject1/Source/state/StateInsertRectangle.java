/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import shapes.Rectangle;
import model.ShapeContainer;
import shapes.ShapeFactory;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateInsertRectangle extends State {

    private static StateInsertRectangle instance = null;

    public void setTransparent(boolean isTransparent){ super.isTransparent = isTransparent; }

    private StateInsertRectangle() {}

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        addToUndoList();
        shapeContainer.getShapes().add(ShapeFactory.getFactoryInstance().createRectangle(point, 35));
        shapeContainer.getLastShape().setIsTransparent(isTransparent);
        shapeContainer.repaint();
    }

    public static StateInsertRectangle getInstance() {
        if (instance == null)
            instance = new StateInsertRectangle();
        return instance;
    }
}
