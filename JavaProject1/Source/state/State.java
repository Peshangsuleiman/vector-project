/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import command.Commander;
import model.ShapeContainer;
import shapes.Point;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public abstract class State{

    protected boolean flag = true;
    protected boolean isTransparent = true;
    protected Commander commander = Commander.getInstance();

    public void pointerDown(Point point, ShapeContainer shapeContainer){};

    public void pointerMoved(Point point, boolean pointerDown, ShapeContainer shapeContainer)  {}

    public void pointerUp(Point point, ShapeContainer shapeContainer){}

    protected void addToUndoList() {commander.addToUndoList(); }

}