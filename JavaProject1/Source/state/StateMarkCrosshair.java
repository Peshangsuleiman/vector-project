/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import shapes.Shape;
import model.ShapeContainer;
import shapes.ShapeDecoratorCrosshair;
import shapes.ShapeFactory;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateMarkCrosshair extends State {

    private static StateMarkCrosshair instance = null;

    private StateMarkCrosshair() {}

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
        if (shapeContainer.selected != null) {
            addToUndoList();
            Shape crossHairShape = ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair(shapeContainer.selected);
            shapeContainer.getShapes().remove(shapeContainer.selected);
            shapeContainer.getShapes().add(crossHairShape);
            shapeContainer.repaint();
        }
    }

    public static StateMarkCrosshair getInstance() {
        if (instance == null)
            instance = new StateMarkCrosshair();
        return instance;
    }
}
