/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Oval;
import shapes.Point;
import model.ShapeContainer;
import shapes.ShapeFactory;

/**
 *
 * @author pesh
 */
public class StateInsertOval extends State{
    
    private static StateInsertOval instance = null;

    public void setTransparent(boolean isTransparent){
        super.isTransparent = isTransparent;
    }

    private StateInsertOval(){
    }
    
    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        addToUndoList();
        shapeContainer.getShapes().add(ShapeFactory.getFactoryInstance().createOval(point, 50.0));
        shapeContainer.getLastShape().setIsTransparent(isTransparent);
        shapeContainer.repaint();
    }

    public static StateInsertOval getInstance(){
        if(instance == null)
            instance = new StateInsertOval();
        return instance; 
    }
    
}
