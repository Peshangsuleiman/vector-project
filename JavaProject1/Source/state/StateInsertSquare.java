/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.ShapeFactory;
import shapes.Square;
import shapes.Point;
import model.ShapeContainer;

/**
 *
 * @author pesh
 */
public class StateInsertSquare extends State {

    private static StateInsertSquare instance = null;

    public void setTransparent(boolean isTransparent){ super.isTransparent = isTransparent; }

    private StateInsertSquare(){ }
    
    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        addToUndoList();
        shapeContainer.getShapes().add(ShapeFactory.getFactoryInstance().createSquare(point, 100));
        shapeContainer.getLastShape().setIsTransparent(isTransparent);
        shapeContainer.repaint();
    }

    public static StateInsertSquare getInstance(){
        if(instance == null)
            instance = new StateInsertSquare();
        return instance; 
    }
}
