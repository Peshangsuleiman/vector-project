/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import shapes.Point;
import shapes.Shape;
import model.ShapeContainer;

/**
 * @author Peshang Suleiman & Ali Shakeri
 */
public class StateUnmark extends State {

    private static StateUnmark instance = null;

    private StateUnmark() {}

    public void pointerDown(Point point, ShapeContainer shapeContainer) {
        shapeContainer.select(point);
        if (shapeContainer.selected != null) {
            addToUndoList();
            Shape unmarkedShape = shapeContainer.selected.peel();
            shapeContainer.getShapes().remove(shapeContainer.selected);
            shapeContainer.getShapes().add(unmarkedShape);
            shapeContainer.repaint();
        }
    }

    public static StateUnmark getInstance() {
        if (instance == null)
            instance = new StateUnmark();
        return instance;
    }
}
