package state;

import model.ShapeContainer;
import shapes.LineFreeHand;
import shapes.Point;

import java.awt.*;

public class StateFreeDraw extends State {

    private static StateFreeDraw instance = null;
    private Color color = Color.BLACK;
    private LineFreeHand line;
    private int lineSize = 1;

    private StateFreeDraw(){}

    public void setColor(Color color){
        this.color = color;
    }

    public void setLinesize(int lineSize){ this.lineSize = lineSize; }

    public void pointerDown(Point point, ShapeContainer shapeContainer){
        shapeContainer.select(point);
    }

    public void pointerMoved(Point point, boolean pointerDown, ShapeContainer shapeContainer) {

        if(pointerDown){
            if (super.flag) {
                addToUndoList();
                line = new LineFreeHand();
                line.setColor(color);
                line.lineSize(lineSize);
                shapeContainer.getShapes().add(line);
                super.flag = false;
            }
            line.moveOrDraw(point.getX(), point.getY());
            shapeContainer.repaint();
        }
    }

    public void pointerUp(Point point, ShapeContainer shapeContainer) {
        shapeContainer.repaint();
        super.flag = true;
    }

    public static StateFreeDraw getInstance(){
        if(instance == null)
            instance = new StateFreeDraw();
        return instance;
    }
}
