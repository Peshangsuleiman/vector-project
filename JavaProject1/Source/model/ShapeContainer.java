package model;

import command.Commander;
import shapes.Point;
import shapes.Pointable;
import shapes.Shape;
import state.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

public class ShapeContainer extends JPanel implements Pointable {

    private static final long serialVersionUID = 1L;
    private List<Shape> shapes = new LinkedList<>();

    private State shapeContainerState;

    public Shape selected;

    public ShapeContainer() {
        super();
        Commander.getInstance().addShapeContainer(this);
        shapeContainerState = NoState.getInstance();
        MouseHandler mouseHandler = new MouseHandler(this);
        KeyListener keyListener = new KeyListener();

        this.addMouseListener(mouseHandler);
        this.addMouseMotionListener(mouseHandler);
        this.addKeyListener(keyListener);
        this.setBackground(Color.white);

        this.setFocusable(true);
        this.requestFocusInWindow();
    }

    public void setState(State state) {
        shapeContainerState = state;
    }

    public List<Shape> getShapes() {
        return shapes;
    }

    public void setShapes(List<Shape> shapes) {
        this.shapes = shapes;
    }

    public Shape getLastShape(){ return shapes.get(shapes.size()-1); }

    //anropas av Swing när det är dags att rendera
    //Is called uppon when repaint() is called.
    //Repaint is called on ShapeContainer in the State that receives it.
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Shape shape : shapes)
            shape.draw((Graphics2D) g);
    }

    public void select(Point point) {
        for (Shape shape : shapes) {
            if (shape.intersects(point)) {
                selected = shape;
            }
        }
    }

    public void pointerDown(Point point)  {
        shapeContainerState.pointerDown(point, this);
    }

    public void pointerUp(Point point) {
        shapeContainerState.pointerUp(point, this);
        selected = null;
    }

    public void pointerMoved(Point point, boolean pointerDown)  {
        shapeContainerState.pointerMoved(point, pointerDown, this);
    }
}