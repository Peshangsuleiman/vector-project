/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import additionalFunctions.StateInsertFilledShapes;
import additionalFunctions.StateInsertTransparentShapes;
import command.Commander;
import filefunctions.Load;
import filefunctions.Save;
import state.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.*;

/*
 *
 *@author Peshang Suleiman & ali Shakeri
 */

public class VectorGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private final JFileChooser fc = new JFileChooser();

    private final ShapeContainer shapeContainer;
    private final JMenu menuFile = new JMenu("File");
    private final JMenu menuShape = new JMenu("Shape");
    private final JMenu menuTools = new JMenu("Tools");
    private final JMenu menuDecorate = new JMenu("Decorate");
    private final JMenu command = new JMenu("Command");
    private final JMenu lineWidth = new JMenu("Linesize");
    private final JMenu paint = new JMenu("Paint");
    private final JMenu transparency = new JMenu("Transparency");

    private final JMenuItem save = new JMenuItem("Save");
    private final JMenuItem load = new JMenuItem("Load");
    private final JMenuItem newWindow = new JMenuItem("New window");
    private final JMenuItem closeWindow = new JMenuItem("Close");
    private final JMenuItem insertRectangle = new JMenuItem("Insert rectangle");
    private final JMenuItem insertCircle = new JMenuItem("Insert circle");
    private final JMenuItem insertSquare = new JMenuItem("Insert square");
    private final JMenuItem insertOval = new JMenuItem("Insert oval");
    private final JMenuItem insertTriangle = new JMenuItem("Insert triangle");
    private final JMenuItem drawFreeLine = new JMenuItem("Draw free line");

    private final JMenuItem Undo = new JMenuItem("Undo");
    private final JMenuItem Redo = new JMenuItem("Redo");

    private final JMenuItem move = new JMenuItem("Move");
    private final JMenuItem delete = new JMenuItem("Delete");
    private final JMenuItem resize = new JMenuItem("Resize");
    private final JMenuItem combine = new JMenuItem("Combine");
    private final JMenuItem split = new JMenuItem("Split");

    private final JMenuItem mark = new JMenuItem("Mark");
    private final JMenuItem crosshairMark = new JMenuItem("Crosshair");
    private final JMenuItem unMark = new JMenuItem("Unmark");

    private final JMenuItem lineWidth1 = new JMenuItem("Ordinary size");
    private final JMenuItem lineWidth2 = new JMenuItem("Size 1");
    private final JMenuItem lineWidth3 = new JMenuItem("Size 2");
    private final JMenuItem lineWidth4 = new JMenuItem("Size 3");
    private final JMenuItem lineWidth5 = new JMenuItem("Size 4");

    private final JMenuItem colorWhite = new JMenuItem( "White");
    private final JMenuItem colorBlue = new JMenuItem( "Blue");
    private final JMenuItem colorRed = new JMenuItem( "Red");
    private final JMenuItem colorGreen = new JMenuItem( "Green");
    private final JMenuItem colorBlack = new JMenuItem( "Black");
    private final JMenuItem colorYellow = new JMenuItem( "Yellow");
    private final JMenuItem colorCyan = new JMenuItem( "Cyan");

    private final JMenuItem transparentOrOpaque = new JMenuItem( "Transparent/Opaque");
    private final JMenuItem insertTransparentShapes = new JMenuItem("Insert transparent shapes");
    private final JMenuItem insertFilledShapes = new JMenuItem("Insert filled shapes");


    public VectorGUI() {
        buildTheUI();
        setUpEventHandling();
        shapeContainer = new ShapeContainer();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(900, 800);
        this.add(shapeContainer);
        this.setVisible(true);
    }

    public void buildTheUI() {

        //Command
        command.add(Undo);
        command.add(Redo);

        ///FILE///
        menuFile.add(newWindow);
        menuFile.add(save);
        menuFile.add(load);
        menuFile.add(closeWindow);

        ///SHAPE///
        menuShape.add(insertRectangle);
        menuShape.add(insertCircle);
        menuShape.add(insertSquare);
        menuShape.add(insertOval);
        menuShape.add(insertTriangle);
        menuShape.add(drawFreeLine);

        ///TOOLS///
        menuTools.add(move);
        menuTools.add(delete);
        menuTools.add(resize);
        menuTools.add(combine);
        menuTools.add(split);
        menuTools.add(unMark);

        ///DECORATE///
        menuDecorate.add(mark);
        menuDecorate.add(crosshairMark);

        ///LINESIZE///
        lineWidth.add(lineWidth1);
        lineWidth.add(lineWidth2);
        lineWidth.add(lineWidth3);
        lineWidth.add(lineWidth4);
        lineWidth.add(lineWidth5);

        //PAINT//
        paint.add(colorWhite);
        paint.add(colorBlack);
        paint.add(colorBlue);
        paint.add(colorGreen);
        paint.add(colorRed);
        paint.add(colorYellow);
        paint.add(colorCyan);

        //TRANSPARENCY//
        transparency.add(transparentOrOpaque);
        transparency.add(insertTransparentShapes);
        transparency.add(insertFilledShapes);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuFile);
        menuBar.add(menuShape);
        menuBar.add(menuTools);
        menuBar.add(menuDecorate);
        menuBar.add(command);
        menuBar.add(lineWidth);
        menuBar.add(paint);
        menuBar.add(transparency);
        this.setJMenuBar(menuBar);
    }


    private void setUpEventHandling() {

        newWindow.addActionListener((ActionEvent e) -> { new VectorGUI(); });
        
        closeWindow.addActionListener((ActionEvent e) -> { System.exit(0); });

        insertRectangle.addActionListener((ActionEvent e) -> shapeContainer.setState(StateInsertRectangle.getInstance()));

        insertCircle.addActionListener((ActionEvent e) -> shapeContainer.setState(StateInsertCircle.getInstance()));

        insertSquare.addActionListener((ActionEvent e) -> shapeContainer.setState(StateInsertSquare.getInstance()));

        insertOval.addActionListener((ActionEvent e) -> shapeContainer.setState(StateInsertOval.getInstance()));

        insertTriangle.addActionListener((ActionEvent e) -> shapeContainer.setState(StateInsertTriangle.getInstance()));

        drawFreeLine.addActionListener((ActionEvent e) -> shapeContainer.setState(StateFreeDraw.getInstance()));

        move.addActionListener((ActionEvent e) -> shapeContainer.setState(StateMove.getInstance()));

        delete.addActionListener((ActionEvent e) -> shapeContainer.setState(StateDelete.getInstance()));

        mark.addActionListener((ActionEvent e) -> shapeContainer.setState(StateMark.getInstance()));

        crosshairMark.addActionListener((ActionEvent e) -> shapeContainer.setState(StateMarkCrosshair.getInstance()));

        unMark.addActionListener((ActionEvent e) -> shapeContainer.setState(StateUnmark.getInstance()));

        resize.addActionListener((ActionEvent e) -> shapeContainer.setState(StateResize.getInstance()));

        Undo.addActionListener((ActionEvent e) -> Commander.getInstance().undoCommand());

        Redo.addActionListener((ActionEvent e) -> Commander.getInstance().redoCommand());

        lineWidth1.addActionListener((ActionEvent e) -> {shapeContainer.setState(StateLineWidth.getInstance());
        StateLineWidth.getInstance().setLineSize(0);
        });
        
        lineWidth2.addActionListener((ActionEvent e) -> {shapeContainer.setState(StateLineWidth.getInstance());
        StateLineWidth.getInstance().setLineSize(1);
        });
         
        lineWidth3.addActionListener((ActionEvent e) -> {shapeContainer.setState(StateLineWidth.getInstance());
        StateLineWidth.getInstance().setLineSize(2);
        });
        
        lineWidth4.addActionListener((ActionEvent e) -> {shapeContainer.setState(StateLineWidth.getInstance());
        StateLineWidth.getInstance().setLineSize(3);
        });
        
        lineWidth5.addActionListener((ActionEvent e) -> {shapeContainer.setState(StateLineWidth.getInstance());
        StateLineWidth.getInstance().setLineSize(4);
        });

        combine.addActionListener((ActionEvent e) -> shapeContainer.setState(new StateCombine()));

        split.addActionListener((ActionEvent e) -> shapeContainer.setState(StateSplit.getInstance()));

        colorWhite.addActionListener((ActionEvent e) -> {
            shapeContainer.setState(StateColor.getInstance());
            StateColor.getInstance().setColor(Color.WHITE);
        });

        colorBlack.addActionListener((ActionEvent e) -> {
            shapeContainer.setState(StateColor.getInstance());
            StateColor.getInstance().setColor(Color.BLACK);
        });

        colorBlue.addActionListener((ActionEvent e) -> {
                shapeContainer.setState(StateColor.getInstance());
        StateColor.getInstance().setColor(Color. BLUE);
        });

        colorGreen.addActionListener((ActionEvent e) -> {
            shapeContainer.setState(StateColor.getInstance());
            StateColor.getInstance().setColor(Color.GREEN);
        });

        colorRed.addActionListener((ActionEvent e) -> {
            shapeContainer.setState(StateColor.getInstance());
            StateColor.getInstance().setColor(Color.RED);
        });

        colorYellow.addActionListener((ActionEvent e) -> {
            shapeContainer.setState(StateColor.getInstance());
            StateColor.getInstance().setColor(Color.YELLOW);
        });

        colorCyan.addActionListener((ActionEvent e) -> {
            shapeContainer.setState(StateColor.getInstance());
            StateColor.getInstance().setColor(Color.CYAN);
        });


        transparentOrOpaque.addActionListener((ActionEvent e) -> shapeContainer.setState(StateSetTransparency.getInstance()));

        insertTransparentShapes.addActionListener((ActionEvent e) -> StateInsertTransparentShapes.getInstance().createTransparentShapes());

        insertFilledShapes.addActionListener((ActionEvent e) -> StateInsertFilledShapes.getInstance().createOpaqueShapes());

        save.addActionListener((ActionEvent e) -> {
            int returnVal = fc.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                new Save(file, shapeContainer);
            } else {
                System.out.println("User canceled file selection");
            }
        });

        load.addActionListener((ActionEvent e) -> {
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                new Load(file, shapeContainer);
            } else {
                System.out.println("User canceled file selection");
            }
        });
    }
}
