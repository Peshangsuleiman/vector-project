package model;

import command.Commander;

import java.awt.*;
import java.awt.event.KeyEvent;

public class KeyListener implements java.awt.event.KeyListener {
    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if ((e.getKeyCode() == KeyEvent.VK_Z) && ((e.getModifiersEx() & Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()) != 0)) {
            Commander.getInstance().undoCommand();
        }
        if ((e.getKeyCode() == KeyEvent.VK_Y) && ((e.getModifiersEx() & Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()) != 0)){
            Commander.getInstance().redoCommand();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {}
}
