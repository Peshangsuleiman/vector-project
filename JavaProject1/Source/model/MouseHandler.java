/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import shapes.Point;
import shapes.Pointable;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 */
//The class is a MouseListener and a MouseMotionListener.
//Hence why the class can be added as such in the ShapeContainer class, since it accepts such listeners.
//In the concrete class MouseHandler we also implement what the methods should do.
//This class is added to the ShapeContainer class since it accepts a MouseListener and MouseMotionListener.
//The Pointable ShapeContainer is sent in so we can use its PointerDown and Mouse dragged/moved functions (assign them).
//pointer methods based on mouse movement (real mouse movement check is done here).
//The e.getX/getY are taken from the container and this is made possible when we add the MouseHandler to the ShapeContainers Listeners.

public class MouseHandler implements MouseListener, MouseMotionListener {

    private Pointable pointable;

    public MouseHandler(Pointable pointable) {
        this.pointable = pointable;
    }

    public void mouseDragged(MouseEvent e) { pointable.pointerMoved(new Point(e.getX(), e.getY()), true);}

    public void mouseMoved(MouseEvent e) { pointable.pointerMoved(new Point(e.getX(), e.getY()), false); }

    public void mouseClicked(MouseEvent e) {}

    public void mousePressed(MouseEvent e) { pointable.pointerDown(new Point(e.getX(), e.getY())); }

    public void mouseReleased(MouseEvent e) { pointable.pointerUp(new Point(e.getX(), e.getY())); }

    public void mouseEntered(MouseEvent e) {}

    public void mouseExited(MouseEvent e) {}
}

