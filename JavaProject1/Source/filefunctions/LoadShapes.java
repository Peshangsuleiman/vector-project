package filefunctions;

import shapes.*;
import shapes.Rectangle;
import shapes.Shape;
import java.awt.*;

public class LoadShapes implements ShapeBlueprint {

    private static LoadShapes instance = null;

    private LoadShapes() {}

    @Override
    public Shape circle(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency) {
        Circle circle = ShapeFactory.getFactoryInstance().createCircle(x, y, radiusOrSide);
        circle.lineSize(lineSize);
        circle.setMarkedShape(markedShapeDec);
        circle.setMarkedCrosshair(markedCrossDec);
        circle.setColor(new Color(color));
        circle.setIsTransparent(transparency);
        return circle;
    }

    @Override
    public Shape oval(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency) {
        Oval oval = ShapeFactory.getFactoryInstance().createOval(x, y, radiusOrSide);
        oval.lineSize(lineSize);
        oval.setMarkedShape(markedShapeDec);
        oval.setMarkedCrosshair(markedCrossDec);
        oval.setColor(new Color(color));
        oval.setIsTransparent(transparency);
        return oval;
    }

    @Override
    public Shape rectangle(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency) {
        Rectangle rectangle = ShapeFactory.getFactoryInstance().createRectangle(x, y, radiusOrSide);
        rectangle.lineSize(lineSize);
        rectangle.setMarkedShape(markedShapeDec);
        rectangle.setMarkedCrosshair(markedCrossDec);
        rectangle.setColor(new Color(color));
        rectangle.setIsTransparent(transparency);
        return rectangle;
    }

    @Override
    public Shape square(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency) {
        Square square = ShapeFactory.getFactoryInstance().createSquare(x, y, radiusOrSide);
        square.lineSize(lineSize);
        square.setMarkedShape(markedShapeDec);
        square.setMarkedCrosshair(markedCrossDec);
        square.setColor(new Color(color));
        square.setIsTransparent(transparency);
        return square;
    }

    @Override
    public Shape triangle(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency) {
        Triangle triangle = ShapeFactory.getFactoryInstance().createTriangle(x, y, radiusOrSide);
        triangle.lineSize(lineSize);
        triangle.setMarkedShape(markedShapeDec);
        triangle.setMarkedCrosshair(markedCrossDec);
        triangle.setColor(new Color(color));
        triangle.setIsTransparent(transparency);
        return triangle;
    }

    public static LoadShapes getInstance() {
        if (instance == null)
            instance = new LoadShapes();
        return instance;
    }
}
