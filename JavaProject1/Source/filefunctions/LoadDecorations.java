package filefunctions;

import shapes.*;
import shapes.Shape;

public class LoadDecorations implements ShapeDecorationsBlueprint {

    private static LoadDecorations instance = null;
    private LoadShapes loadShapes;

    private LoadDecorations() {
        loadShapes =  LoadShapes.getInstance();
    }

    public Shape createDecoratedShapes(double x, double y, double radiusOrSide, int linSize, String markedShapeDec,
                                       String markedCrossDec, String subType, int color, boolean transparency) {

        switch (subType) {
            case "CIRCLE":
                return decoratedCircle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency);
            case "OVAL":
                return decorateOval(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency);
            case "RECTANGLE":
                return decorateRectangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency);
            case "SQUARE":
                return decorateSquare(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency);
            case "TRIANGLE":
                return decorateTriangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency);
            default:
                return null;
        }
    }


    public Shape decoratedCircle(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency){

        if (markedShapeDec.equals("SD") && markedCrossDec.equals("N"))
            return ShapeFactory.getFactoryInstance().createShapeDecorator
                    (loadShapes.circle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else if (markedShapeDec.equals("N") && markedCrossDec.equals("SDC"))
            return ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.circle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else
            return ShapeFactory.getFactoryInstance().createShapeDecorator(ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.circle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency)));
    }

    public Shape decorateOval(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency){

        if (markedShapeDec.equals("SD") && markedCrossDec.equals("N"))
            return ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.oval(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else if (markedShapeDec.equals("N") && markedCrossDec.equals("SDC"))
            return ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.oval(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else
            return ShapeFactory.getFactoryInstance().createShapeDecorator(ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.oval(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency)));
    }

    public Shape decorateRectangle(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency){

        if (markedShapeDec.equals("SD") && markedCrossDec.equals("N"))
            return ShapeFactory.getFactoryInstance().createShapeDecorator
                    (loadShapes.rectangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else if (markedShapeDec.equals("N") && markedCrossDec.equals("SDC"))
            return ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.rectangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else
            return ShapeFactory.getFactoryInstance().createShapeDecorator(ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.rectangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency)));
    }

    public Shape decorateSquare(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency){

        if(markedShapeDec.equals("SD") && markedCrossDec.equals("N"))
            return ShapeFactory.getFactoryInstance().createShapeDecorator
                    (loadShapes.square(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else if (markedShapeDec.equals("N") && markedCrossDec.equals("SDC"))
            return ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.square(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else
            return ShapeFactory.getFactoryInstance().createShapeDecorator(ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.square(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency)));
    }

    public Shape decorateTriangle(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency){

        if (markedShapeDec.equals("SD") && markedCrossDec.equals("N"))
            return ShapeFactory.getFactoryInstance().createShapeDecorator
                    (loadShapes.triangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else if (markedShapeDec.equals("N") && markedCrossDec.equals("SDC"))
            return ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair(loadShapes.triangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency));
        else
            return ShapeFactory.getFactoryInstance().createShapeDecorator(ShapeFactory.getFactoryInstance().createShapeDecoratorCrosshair
                    (loadShapes.triangle(x, y, radiusOrSide, linSize, markedShapeDec, markedCrossDec, color, transparency)));
    }

    public static LoadDecorations getInstance() {
        if (instance == null)
            instance = new LoadDecorations();
        return instance;
    }
}
