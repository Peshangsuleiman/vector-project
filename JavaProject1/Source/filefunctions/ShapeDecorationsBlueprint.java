package filefunctions;

import shapes.Shape;

public interface ShapeDecorationsBlueprint {

    Shape createDecoratedShapes(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, String subType, int color, boolean transparency);

    Shape decoratedCircle(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);

    Shape decorateOval(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);

    Shape decorateRectangle(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);

    Shape decorateSquare(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);

    Shape decorateTriangle(double x, double y, double radiusOrSide, int linSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);
}
