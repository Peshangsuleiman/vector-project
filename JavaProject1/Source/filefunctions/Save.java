package filefunctions;

import shapes.Shape;
import model.ShapeContainer;

import java.io.*;



public class Save {

   private ShapeContainer shapeContainer;
   private File saveFile;

    public Save(File saveFile, ShapeContainer shapeContainer) {
        this.shapeContainer = shapeContainer;
        this.saveFile = saveFile;
        saveShapes();
    }

    private void saveShapes() {
        //if statement here to if its a TYPE freeShape do a forEach loop and get all the pixel values.
        //and in load if the typ is free shape us different read method.
        try {
            FileOutputStream fileOS = new FileOutputStream(saveFile.getPath());
            PrintWriter pw = new PrintWriter(fileOS);
            for (Shape shape : shapeContainer.getShapes()) {
                pw.println(shape.getType());
                pw.println(shape.getPosition().getX());
                pw.println(shape.getPosition().getY());
                pw.println(shape.getOriginalSideOrRadius());
                pw.println(shape.getLineSize());
                pw.println(shape.getMarkedShape());
                pw.println(shape.getMarkedCrosshair());
                pw.println(shape.getSubType());
                pw.println(shape.getColor());
                pw.println(shape.getIsTransparent());
                pw.println();
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}