package filefunctions;

import model.ShapeContainer;
import shapes.Shape;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Load {

    private final ShapeContainer shapeContainer;
    private final File loadFile;
    private final List<Shape> shapeList;
    private final LoadDecorations shapeDecoratorLoader;
    private final LoadShapes shapeLoader;

    public Load(File loadFile, ShapeContainer shapeContainer) {
        this.shapeContainer = shapeContainer;
        this.loadFile = loadFile;
        shapeDecoratorLoader = LoadDecorations.getInstance();
        shapeLoader = LoadShapes.getInstance();
        shapeList = new ArrayList<>();
        loadShapes();
    }

    private void loadShapes(){
        try {
            FileInputStream fileIS = new FileInputStream(loadFile);
            Scanner shapeFile = new Scanner(fileIS);
            while (shapeFile.hasNextLine()){
                    shapeList.add(shapeCreatorM(shapeFile.nextLine(), Double.parseDouble(shapeFile.nextLine()), Double.parseDouble(shapeFile.nextLine()),
                            Double.parseDouble(shapeFile.nextLine()), Integer.parseInt(shapeFile.nextLine()), shapeFile.nextLine(),
                            shapeFile.nextLine(), shapeFile.nextLine(), Integer.parseInt(shapeFile.nextLine()), Boolean.parseBoolean(shapeFile.nextLine())));
                    shapeFile.nextLine();
            }
            shapeContainer.setShapes(shapeList);
            shapeContainer.repaint();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

   private Shape shapeCreatorM(String type, double x, double y, double radiusOrSide, int lineSize,
                               String markedShapeDec, String markedCrossDec, String subType, int color, boolean transparency){
        switch(type) {
            case "CIRCLE":
                return shapeLoader.circle(x, y, radiusOrSide, lineSize, markedShapeDec, markedCrossDec, color, transparency);
            case "OVAL":
                return shapeLoader.oval(x, y, radiusOrSide, lineSize, markedShapeDec, markedCrossDec, color, transparency);
            case "RECTANGLE":
                return shapeLoader.rectangle(x, y, radiusOrSide, lineSize, markedShapeDec, markedCrossDec, color, transparency);
            case "SQUARE":
                return shapeLoader.square(x, y, radiusOrSide, lineSize, markedShapeDec, markedCrossDec, color, transparency);
            case "TRIANGLE":
                return shapeLoader.triangle(x, y, radiusOrSide, lineSize, markedShapeDec, markedCrossDec, color, transparency);
            default:
                return shapeDecoratorLoader.createDecoratedShapes(x, y, radiusOrSide, lineSize, markedShapeDec, markedCrossDec, subType, color, transparency);
        }
    }
}
