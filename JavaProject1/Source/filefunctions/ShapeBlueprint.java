package filefunctions;

import shapes.Shape;

public interface ShapeBlueprint {

    Shape circle(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);
    Shape oval(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);
    Shape rectangle(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);
    Shape square(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);
    Shape triangle(double x, double y, double radiusOrSide, int lineSize, String markedShapeDec, String markedCrossDec, int color, boolean transparency);
}
