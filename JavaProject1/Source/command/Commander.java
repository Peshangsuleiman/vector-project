package command;

import model.*;
import shapes.Shape;
import java.util.ArrayList;
import java.util.List;

public class Commander {

    private static Commander instance = null;
    private final List<List<Shape>> undoList;
    private final List<List<Shape>> redoList;
    private ShapeContainer shapeContainer;
    private boolean firstLastIndex = true;

    public Commander() {
        undoList = new ArrayList<>();
        redoList = new ArrayList<>();
    }

    public void addShapeContainer(ShapeContainer shapeContainer) {
        this.shapeContainer = shapeContainer;
    }

    public List<Shape> getLastListInUndoList() {
        return undoList.getLast();
    }

    public List<Shape> getLastListInRedoList(){
        return redoList.getLast();
    }


    public void addToUndoList()  {
        List<Shape> newList = new ArrayList<>();
        for (Shape shape : shapeContainer.getShapes()) {
            newList.add(shape.shapeClone());
        }
        undoList.add(newList);
        redoList.clear();
    }

    public void undoCommand() {
        if (!undoList.isEmpty()) {
            if (firstLastIndex) {
                redoList.add(shapeContainer.getShapes());
                redoList.add(getLastListInUndoList());
                shapeContainer.setShapes(getLastListInUndoList());
                undoList.removeLast();
                firstLastIndex = false;
            } else {
                redoList.add(getLastListInUndoList());
                shapeContainer.setShapes(getLastListInUndoList());
                undoList.removeLast();
            }
            shapeContainer.repaint();
        }
    }

    public void redoCommand() {
        if (!redoList.isEmpty()) {
            undoList.add(new ArrayList<>(getLastListInRedoList()));
            shapeContainer.setShapes(getLastListInRedoList());
            redoList.removeLast();
            shapeContainer.repaint();
            if(redoList.isEmpty())
                firstLastIndex = true;
        }
    }

    public static Commander getInstance() {
        if (instance == null)
            instance = new Commander();
        return instance;
    }
}

