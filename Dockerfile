FROM openjdk:21-jdk-slim
WORKDIR /app
COPY JavaProject1/Source/ ./
RUN find . -name "*.java" -exec javac {} +
CMD ["java", "main.APDraw"]